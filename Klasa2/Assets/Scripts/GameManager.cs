﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager: MonoBehaviour
{

    int numerLekcji;
    int iloscUkonczonychLekcji;
    int numerZadania;
    private void Awake()
    {
        if(FindObjectsOfType<GameManager>().Length > 1)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        if (!PlayerPrefs.HasKey("SaveIloscUkonczonychLekcji"))
        {
            PlayerPrefs.SetInt("SaveIloscUkonczonychLekcji",0);
            iloscUkonczonychLekcji = 0;
        }
        else
        {
            iloscUkonczonychLekcji = PlayerPrefs.GetInt("SaveIloscUkonczonychLekcji");

        }

        if (!PlayerPrefs.HasKey("SaveNumerZadania"))
        {
            PlayerPrefs.SetInt("SaveNumerZadania", 0);
            numerZadania = 0;
        }
        else
        {
            numerZadania = PlayerPrefs.GetInt("SaveNumerZadania");

        }

    }

    public int PobierzIloscUkonczonychLekcji()
    {
        return PlayerPrefs.GetInt("SaveIloscUkonczonychLekcji");
    }

    public void DodajUkonczonaLekcje()
    {
        iloscUkonczonychLekcji = PlayerPrefs.GetInt("SaveIloscUkonczonychLekcji");
        iloscUkonczonychLekcji++;
        PlayerPrefs.SetInt("SaveIloscUkonczonychLekcji", iloscUkonczonychLekcji);
        
    }

    public int PobierzNumerLekcji()
    {
        return numerLekcji;
    }
  
    public int PobierzNumerZadania()       
    {
        return PlayerPrefs.GetInt("SaveNumerZadania");
    }


    public void ZwiekszNumerZadania()
    {
        numerZadania = PlayerPrefs.GetInt("SaveNumerZadania");
        numerZadania++;
        PlayerPrefs.SetInt("SaveNumerZadania", numerZadania);

    }

    public void PobierzNumerLekcjiZPrzycisku(int nrLekcji)
    {
        numerLekcji = nrLekcji;
        PrzelaczScene(1);
    }

    public void PrzelaczScene(int sceneBuildIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex);
    }


    public void ResetujIloscUkonczonychLekcji()
    {
        PlayerPrefs.SetInt("SaveIloscUkonczonychLekcji", 0);
    }

    public void ResetujNumerZadania()
    {
        PlayerPrefs.SetInt("SaveNumerZadania", 0);
    }


}
