﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Punkty : MonoBehaviour
{
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("SavePunkty"))
        {
            PlayerPrefs.SetInt("SavePunkty", 0);
            iloscPunktow = 0;
        }
        else
        {
            iloscPunktow = PlayerPrefs.GetInt("SavePunkty");
        }
        if (!PlayerPrefs.HasKey("SavePunktyWAktualnejLekcji"))
        {
            PlayerPrefs.SetInt("SavePunktyWAktualnejLekcji", 0);
            iloscPunktowAktualnejLekcji = 0;
        }
        else
        {
            iloscPunktow = PlayerPrefs.GetInt("SavePunktyWAktualnejLekcji");
        }
    }
    int iloscPunktow;
    int iloscPunktowAktualnejLekcji;

    public void DodajPunkty(int punkty)
    {
        iloscPunktow = PlayerPrefs.GetInt("SavePunkty");
        iloscPunktow += punkty;
        PlayerPrefs.SetInt("SavePunkty", iloscPunktow);
        

        iloscPunktowAktualnejLekcji = PlayerPrefs.GetInt("SavePunktyWAktualnejLekcji");
        iloscPunktowAktualnejLekcji += punkty;
        PlayerPrefs.SetInt("SavePunktyWAktualnejLekcji", iloscPunktowAktualnejLekcji);
    }

    public int PobierzIloscPunktow()
    {
        return PlayerPrefs.GetInt("SavePunkty");
    }

    public int PobierzIloscPunktowWAktualnejLekcji()
    {
        return PlayerPrefs.GetInt("SavePunktyWAktualnejLekcji");
    }

    public void ResetujIloscPunktow()
    {
        PlayerPrefs.SetInt("SavePunkty", 0);
    }
    public void ResetujIloscPunktowWAktualnejLekcji()
    {
        PlayerPrefs.SetInt("SavePunktyWAktualnejLekcji", 0);
    }
}
