﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Zadanie", menuName = "Utworz nowe zadanie")] 
public class Zadanie : ScriptableObject
{
    [SerializeField] int typZadania;
    [TextArea(10,100)]
    [SerializeField] string trescZadania;
    [SerializeField] string prawidlowaOdpowiedz;
    [SerializeField] string zlaOdpowiedz1;
    [SerializeField] string zlaOdpowiedz2;
    [SerializeField] string zlaOdpowiedz3;
    [SerializeField] Sprite zdjecieDoZzadania;
   
    public Sprite PobierzZdjeciaDoZadania()
    {
        return zdjecieDoZzadania;
    }

    public string PobierzTrescZadania()
    {
        return trescZadania;
    }
    
   public string PobierzPrawidlowaOdp()
    {
        return prawidlowaOdpowiedz;
    }

    public string PobierzZlaOdp(int numerOdp)
    {
       
        if (numerOdp ==1)
        {
            return zlaOdpowiedz1;
        }else if(numerOdp ==2)
        {
            return zlaOdpowiedz2;
        }
        else if (numerOdp ==3)
        {
            return zlaOdpowiedz3;
        }
        else
        {
        return "Blad";
        }

    }
}
