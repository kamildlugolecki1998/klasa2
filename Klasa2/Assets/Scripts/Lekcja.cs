﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lekcja : MonoBehaviour
{

    [SerializeField] Text txtTrescZadania;
    [SerializeField] Text txtNumerZadania;
    [SerializeField] Text txtNumerLekcji;
    [SerializeField] Text txtIloscPunktow;
    [SerializeField] Button[] przyciskiOdp;
    [SerializeField] Zadanie[] zadaniaLekcja0;
    [SerializeField] Zadanie[] zadaniaLekcja1;
    [SerializeField] Zadanie[] zadaniaLekcja2;
    [SerializeField] GameObject panelUkonczeniaLekcji;
    [SerializeField] Image imgZdjecieDoZadania;
    [SerializeField] GameObject panelZPrzyciskami;
    [SerializeField] GameObject panelZInputField;
    [SerializeField] Text txtPodsumowanieLekcji;
    

    Zadanie[] zadaniaAktualniejLekcji;
    Punkty punktySkrypt;
    GameManager gm;
    SoundManager sm;
    Slider postep;
    bool czyToJestPierwszaOdp;
    int numerZadania;
    int numerLekcji = 0;
    string odpowiedzUzytkownika;
    int ileJestOdpowiedziWZadaniu;
    void Start()
    {

        OdkryPanelInputField(false);
        OdkryPanelZPrzyciskami(false);
        czyToJestPierwszaOdp = true;
        postep = FindObjectOfType<Slider>();
        sm = FindObjectOfType<SoundManager>();
        gm = FindObjectOfType<GameManager>();
        numerLekcji = gm.PobierzNumerLekcji();
        UstawLekcje(numerLekcji - 1);
        punktySkrypt = FindObjectOfType<Punkty>();

        if (numerLekcji == gm.PobierzIloscUkonczonychLekcji())
        {
            numerZadania = gm.PobierzNumerZadania();
        }else
        {
            numerZadania = 0;
        }

        TworzNoweZadanie();
        WyswietlNumerZadania();
        WyswietlIloscPunktow();
        print(numerLekcji  + "numer lekcji");
        print(gm.PobierzIloscUkonczonychLekcji()+ " ilosc ukonczonych lekcji");
        print("Numer zadania zapisanego" + gm.PobierzNumerZadania());
        
    }

    void OdkryPanelZPrzyciskami(bool odkryj)
    {
       panelZPrzyciskami.SetActive(odkryj);
        
    }
    void OdkryPanelInputField(bool odkryj)
    {
        panelZInputField.SetActive(odkryj);

    }

    void DodajPodsumowanieDoPaneluKonczacegoLekcje()
    {
        string podsumowanie = "Uzyskano " + punktySkrypt.PobierzIloscPunktowWAktualnejLekcji() + "  z " + zadaniaAktualniejLekcji.Length + " możliwych punktów";
        txtPodsumowanieLekcji.text = podsumowanie;
    }    
    void UstawLekcje(int nrLekcji)
    {
        numerLekcji = nrLekcji;
        txtNumerLekcji.text =  (numerLekcji +1).ToString();
        switch (nrLekcji)
        {
            case 0:  
                zadaniaAktualniejLekcji = zadaniaLekcja0;
                break;
            case 1:
                zadaniaAktualniejLekcji = zadaniaLekcja1;
                break;
            case 2:
                zadaniaAktualniejLekcji = zadaniaLekcja2;
                break;

            default:
                zadaniaAktualniejLekcji = zadaniaLekcja0;
                break;
        }
        
    }
    void SprawdzIleJestOdpowiedziWZadaniu()
    {
        ileJestOdpowiedziWZadaniu = 0;
        for (int i = 0; i < 3; i++)
        {
           if(zadaniaAktualniejLekcji[numerZadania].PobierzZlaOdp(i + 1) != "")
            {
                ileJestOdpowiedziWZadaniu++;
            }
        }
        ileJestOdpowiedziWZadaniu++;
          
    }

    void WyswietlNumerZadania()
    {
        txtNumerZadania.text =  (numerZadania+1).ToString();
    }

    void UkryjWszystkiePrzyciksiOdpowiedzi()
    {
        for (int i = 0; i < przyciskiOdp.Length; i++)
        {
            przyciskiOdp[i].gameObject.SetActive(false);
        }
    }

    void OdkryjPrzyciskiOdp()
    {
        for (int i = 0; i < ileJestOdpowiedziWZadaniu; i++)
        {
            przyciskiOdp[i].gameObject.SetActive(true);
        }
    }

    void TworzNoweZadanie()
    {
        odpowiedzUzytkownika = "___";
        UkryjWszystkiePrzyciksiOdpowiedzi();
        SprawdzIleJestOdpowiedziWZadaniu();
        if (ileJestOdpowiedziWZadaniu == 1)
        {
            OdkryPanelInputField(true);
            OdkryPanelZPrzyciskami(false);
        }
        else
        {
            OdkryPanelZPrzyciskami(true);
            OdkryPanelInputField(false);
            OdkryjPrzyciskiOdp();
            PrzypiszWartosciDoPrzyciskow();
        }

        PrzypiszElementyDoZadania();
        WyswietlNumerZadania();
        PrzypiszZdjecieDoZadania();
        UstawSlider();
    }

    void PrzypiszZdjecieDoZadania()
    {
        if (zadaniaAktualniejLekcji[numerLekcji].PobierzZdjeciaDoZadania() != null)
        {
            imgZdjecieDoZadania.sprite = zadaniaAktualniejLekcji[numerLekcji].PobierzZdjeciaDoZadania();
        }
    }

    void PrzypiszElementyDoZadania()
    {
        txtTrescZadania.text = zadaniaAktualniejLekcji[numerZadania].PobierzTrescZadania().Replace("__", odpowiedzUzytkownika);
    }


    public void PobierzOdpowiedzUzytkownika(string odp)
    {
        odpowiedzUzytkownika = odp; 
        PrzypiszElementyDoZadania();
    }    
    
    public void SprawdzOdpowiedz()
    {
      
       
        if (odpowiedzUzytkownika == zadaniaAktualniejLekcji[numerZadania].PobierzPrawidlowaOdp() || panelZInputField.GetComponentInChildren<InputField>().text.ToLower().Trim() == zadaniaAktualniejLekcji[numerZadania].PobierzPrawidlowaOdp())
        {
            
            //  print("Odpowiedz Poprawna");
            if (numerLekcji == gm.PobierzIloscUkonczonychLekcji())
            {
                if (czyToJestPierwszaOdp)
                {
                punktySkrypt.DodajPunkty(1);

                }
               
            }
            sm.OdtworzDzwiek(0);
            WyswietlIloscPunktow();
            UstawSlider();
            PrzelaczNastepneZadanie();
            
        }
        else
        {
            print("Odpowiedz NIEpoprawna");
            czyToJestPierwszaOdp = false;
        }

        panelZInputField.GetComponentInChildren<InputField>().text = "";
    }

    void PrzelaczNastepneZadanie()
    {
        czyToJestPierwszaOdp = true;
        if (numerZadania < zadaniaAktualniejLekcji.Length - 1)
        {
             numerZadania++;
             gm.ZwiekszNumerZadania();
            
            TworzNoweZadanie();
            

        }else
        {
            
            ZakonczLekcje();
 
        }
    }
    void ZakonczLekcje()
    {
        
        AktywujPanelUkonczeniaLekcji(true);
        if (numerLekcji == gm.PobierzIloscUkonczonychLekcji())
        {
            gm.DodajUkonczonaLekcje();
            gm.ResetujNumerZadania();
        }
        
        sm.OdtworzDzwiek(1);
    }
    void AktywujPanelUkonczeniaLekcji(bool odkryj)
    {
        DodajPodsumowanieDoPaneluKonczacegoLekcje();
        panelUkonczeniaLekcji.SetActive(odkryj);
        punktySkrypt.ResetujIloscPunktowWAktualnejLekcji();

    }

    public void ZamknijPanelUkonczeniaLekcji()
    {
        
        AktywujPanelUkonczeniaLekcji(false);
        PrzejdzDoMenu();
    }

    void PrzypiszWartosciDoPrzyciskow()
    {
        List<string> listaOdpowiedzi = new List<string>();
        for (int i = 0; i < ileJestOdpowiedziWZadaniu; i++)
        {
            if (i == 0)
            {
                listaOdpowiedzi.Add(zadaniaAktualniejLekcji[numerZadania].PobierzPrawidlowaOdp());
            }
            else
            {
                listaOdpowiedzi.Add(zadaniaAktualniejLekcji[numerZadania].PobierzZlaOdp(i));
            }
        }
        int j = 0;
        for (int i = listaOdpowiedzi.Count;  i > 0; i--)
        {
            int rand = Random.Range(0, listaOdpowiedzi.Count);

            przyciskiOdp[j].GetComponentInChildren<Text>().text = listaOdpowiedzi[rand];
            listaOdpowiedzi.RemoveAt(rand);
            j++;
        }

    }
    public void PrzejdzDoMenu()
    {
        gm.PrzelaczScene(0);
    }

    void WyswietlIloscPunktow()
    {
        txtIloscPunktow.text =  punktySkrypt.PobierzIloscPunktow().ToString();
    }
    void UstawSlider()
    {
        postep.maxValue = zadaniaAktualniejLekcji.Length;
        postep.value = numerZadania;
        
    }
}
