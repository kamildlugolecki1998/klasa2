﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PrzyciskOdpowiedzi : MonoBehaviour
{
    Text txtButton;

    void Start()
    {
        txtButton = GetComponentInChildren<Text>();
        GetComponent<Button>().onClick.AddListener(() => FindObjectOfType<Lekcja>().PobierzOdpowiedzUzytkownika(txtButton.text));
    }

  
}
