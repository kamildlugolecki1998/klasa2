﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Menu : MonoBehaviour
{

    [SerializeField] GameObject btnPrefab;
    [SerializeField] Transform prefabRodzic;

    [SerializeField]GameObject panelUstawien;

    int iloscWszystkichDostepnychLekcji = 3;
    void Start()
    {
        StworzPrzyciskiWyboruLekcji();
    }

    

    void StworzPrzyciskiWyboruLekcji()
    {
        for (int i = 0; i < iloscWszystkichDostepnychLekcji; i++)
        {
            GameObject btn = Instantiate(btnPrefab, prefabRodzic);
            btn.GetComponentInChildren<Text>().text = (i + 1).ToString();
        }
    }

    public void ResetujPlayerPrefsy()
    {
        FindObjectOfType<GameManager>().ResetujIloscUkonczonychLekcji();
        FindObjectOfType<GameManager>().ResetujNumerZadania();
        FindObjectOfType<Punkty>().ResetujIloscPunktow();
        FindObjectOfType<Punkty>().ResetujIloscPunktowWAktualnejLekcji();
        SceneManager.LoadScene(0);
    }

    public void Zamknij()
    {
        Application.Quit();
    }


    public void ZamknijPanelUstawien(bool odkryj)
    {
        panelUstawien.SetActive(odkryj);
    }
}
